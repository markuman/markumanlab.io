# Summary

- [INDEX](./README.md)
- [2019](./2019/summary.md)
    - [hcloud compressed volumes](./2019/hcloud_compressed_volumes.md)
    - [serverless ansible playbooks](./2019/serverless_ansible_playbooks.md)
- [2018](./2018/summary.md)
    - [Docker Swarm Resolver](./2018/docker_swarm_resolver.md)
    - [Docker Swarm, UFW and iptables](./2018/docker_swarm_ufw_iptables.md)
    - [Docker Swarm Loadbalancing](./2018/docker_swarm_loadbalancing.md)
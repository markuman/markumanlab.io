## Serverless Ansible Tasks on AWS

When you start scripting your infrastructure using ansible, you might come up with the idea to schedule some ansible tasks periodically.  
Lambda seems a perfect place for it, because it can execute Python and Ansible is written in Python. 

After a short while you'll notice that it is not that easy to invoke a playbook directly from python. So you end up with a quick'n'dirty subprocess `call` to invoke the playbook. It works fine - locally.

```python
from subprocess import call
call(["ansible-playbook", playbook])
```

After packing everything for lambda and do a test invoke, all what lambda responses is:

```
ERROR! Unable to use multiprocessing, this is normally caused by lack of access to /dev/shm: [Errno 38] Function not implemented
```

That's modest and you can nothing do about it to fix this - and possibly AWS won't ever fix it too.  

My workaround is to put everything into a Dockerfile and run this with AWS Fargate (serverless container execution).


```Dockerfile
FROM alpine:edge

RUN apk --update --no-cache add ansible python3
RUN pip3 install boto3

COPY some_infrastructure.yml /

CMD ansible-playbook some_infrastructure.yml
```

For me my playbook execution time is between 17 and 23 seconds. When it is triggered one time per day, the costs are:  0.00376464 USD.

```
> 23 * (0.25 * 0.00001406 + 0.5 * 0.00000353) * 31
0.00376464
```

That's near to nothing...and you can even invoke it from lambda to benefit from all triggers which are available.

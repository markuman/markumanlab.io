## Docker Swarm UFW iptables

When you're running docker swarm mode, you're facing the problem that iptable rules of ufw has no affect on published ports.  
This is right and a corrected behaviour and also [well documentated.](https://docs.docker.com/network/iptables/)

There are sevaral blog entries, github projects, stackoverflow answers and so one, which are fixing this issue by disabling iptable manipulation by docker and routes everything by there own. I bet that most people who blindly copy/paste those hacks, have no clue what they're doing.

### It's so damn simple

**1st.** simple only expose ports you want to serve public :)  
   Seriously. Example: You need to make a dump from your MariaDB, which runs in the docker network `db`.  
   Just run the dump from another docker container which is attached to the `db` network, has the backup destination mounted as a volume, start `xtrabackup` and you're done.

When you're not able to fulfill the first rule, just follow the 2nd rule.

**2nd.** simple add a iptables rule to the `DOCKER-USER` chain.  
   This chain is evaluated before docker set the iptables. Sadly, ufw is not able to apply rules in different chains. But don't worry, you can do it KISS with ansible

```
- name: drop public fluentd port
    become: yes
    iptables:
        action: insert
        chain: DOCKER-USER
        protocol: tcp
        destination_port: 24224
        jump: DROP
        ip_version: "ipv4"
        comment: drop fluentd port
```

... and you're done.

### disadvantages

Yes, every solution in IT - **every** - has advantages and disadvantages.  
When the server is rebooting, the added rules in the `DOCKER-USER` chain are gone!

The best idea is to use a simple systemd unit file, which invokes your ansible playbook to re-apply your addition iptables rules for the `DOCKER-USER` chain on reboot.

```
[Unit]
Description=Firewall configuration script
After=docker.service
Requires=docker.service

[Service]
Type=simple
ExecStart=/usr/local/bin/ansible-playbook /path/to/playbook/iptable.yml
Restart=on-failure
RestartSec=2

[Install]
WantedBy=basic.target
```

### And UFW?

**3rd** rule: Just use ufw for everything which is not deployed via docker.

---

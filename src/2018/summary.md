# summary 2018

- [Docker Swarm Resolver](./2018/docker_swarm_resolver.md)
- [Docker Swarm, UFW and iptables](./2018/docker_swarm_ufw_iptables.md)
- [Docker Swarm Loadbalancing](./2018/docker_swarm_loadbalancing.md)